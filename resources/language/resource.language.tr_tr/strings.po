# Kodi Media Center language file
# Addon Name: Libre.fm Scrobbler
# Addon id: service.scrobbler.librefm
# Addon Provider: Team-Kodi
msgid ""
msgstr ""
"Project-Id-Version: XBMC Addons\n"
"Report-Msgid-Bugs-To: translations@kodi.tv\n"
"POT-Creation-Date: YEAR-MO-DA HO:MI+ZONE\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Kodi Translation Team\n"
"Language-Team: Turkish (http://www.transifex.com/projects/p/xbmc-addons/language/tr/)\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

msgctxt "Addon Summary"
msgid "Scrobble service for libre.fm"
msgstr ""

msgctxt "Addon Description"
msgid "Scrobbler will submit info of the songs you've been listening to in Kodi to libre.fm"
msgstr ""

msgctxt "#30000"
msgid "General"
msgstr "Genel"

# empty strings from id 30001 to 30003
msgctxt "#30004"
msgid "Submit songs to Libre.fm"
msgstr "Şarkıları Libre.fm'e gönder"

msgctxt "#30005"
msgid "Submit streaming radio to Libre.fm"
msgstr ""

msgctxt "#30006"
msgid "Libre.fm username"
msgstr "Libre.fm kullanıcı adı"

msgctxt "#30007"
msgid "Libre.fm password"
msgstr "Libre.fm parolası"

# empty strings with id 30008
msgctxt "#30009"
msgid "Enable logging"
msgstr ""

# empty strings from id 300010 to 32000
msgctxt "#32001"
msgid "Authentication error"
msgstr ""

msgctxt "#32002"
msgid "Invalid time, please adjust your system clock and restart Kodi"
msgstr ""

msgctxt "#32003"
msgid "Please update the add-on"
msgstr ""
